# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fullcontact', '0003_auto_20150512_1335'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticket',
            name='ticket',
            field=models.TextField(default=''),
        ),
    ]
