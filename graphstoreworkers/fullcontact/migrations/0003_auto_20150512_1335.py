# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fullcontact', '0002_ticket_text'),
    ]

    operations = [
        migrations.RenameField(
            model_name='ticket',
            old_name='text',
            new_name='ticket_id',
        ),
    ]
