# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fullcontact', '0005_batchemail'),
    ]

    operations = [
        migrations.AlterField(
            model_name='batchemail',
            name='email',
            field=models.TextField(default='', unique=True),
        ),
    ]
