# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fullcontact', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticket',
            name='text',
            field=models.TextField(default=''),
        ),
    ]
