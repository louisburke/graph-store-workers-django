# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fullcontact', '0004_ticket_ticket'),
    ]

    operations = [
        migrations.CreateModel(
            name='BatchEmail',
            fields=[
                ('id', models.AutoField(primary_key=True, verbose_name='ID', auto_created=True, serialize=False)),
                ('email', models.TextField(default='')),
            ],
        ),
    ]
