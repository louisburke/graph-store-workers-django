import hashlib
import os

from datetime import date
from json import dumps
from mock import patch

from django.conf import settings
from django.shortcuts import render_to_response
from django.test import TestCase

from fullcontact.models import Ticket, BatchEmail

from .forms import UploadFileForm
from .views import (
    get_s3_path,
    write_data_to_s3,
    batch_func,
    upload_func,
    webhook_func,
    sqs_push
)

from .test_ticket import test_ticket
test_dict = {
  "result": '{"email":"x@x.com"}',
  "webhookId": "123456789"
}
JSON_DIR = 'tito_and_full_contact_json_tickets'


class FakeDate(date):
    "A fake replacement for date that can be mocked for testing."
    def __new__(cls, *args, **kwargs):
        return date.__new__(date, *args, **kwargs)


class FullContactServerTest(TestCase):

    @patch('fullcontact.views.date', FakeDate)
    def test_get_s3_path(self):
        from datetime import date
        FakeDate.today = classmethod(lambda cls: date(2011, 7, 3))

        json_dir = JSON_DIR + '/' + str(FakeDate.today())
        ticket_json = dumps(test_ticket, sort_keys=True)
        hash_out = hashlib.sha512(ticket_json.encode('utf-8'))
        ticket_hash = hash_out.hexdigest()
        test_path = "{}/{}.json".format(json_dir, ticket_hash)

        path = get_s3_path(test_ticket)
        self.assertEqual(test_path, path)

    @patch('fullcontact.views.get_s3_connection')
    def test_write_to_s3_data(self, mock_get_s3_connection):
        data = ''
        bucket_name = 's3-data'
        write_data_to_s3(data, bucket_name)
        self.assertTrue(mock_get_s3_connection.called)

    @patch('fullcontact.views.write_data_to_s3')
    @patch('fullcontact.views.sqs_push')
    def test_batch_func(self, mock_write_data_to_s3, mock_sqs_push):
        fc_data = '{"fullcontact":"details"}'
        BatchEmail.objects.create(email='test@test.com')
        batch_func(fc_data, dumps(test_ticket['tito']))
        self.assertTrue(mock_write_data_to_s3.called)
        self.assertTrue(mock_sqs_push.called)

    @patch('fullcontact.views.write_data_to_s3')
    @patch('fullcontact.views.sqs_push')
    def test_webhook_func(self, mock_write_data_to_s3, mock_sqs_push):
        fc_data = '{"fullcontact":"details"}'
        Ticket.objects.create(ticket_id='1234567',
                              ticket=dumps(test_ticket['tito']))
        webhook_func(fc_data, '1234567')
        self.assertTrue(mock_write_data_to_s3.called)
        self.assertTrue(mock_sqs_push.called)

    @patch('fullcontact.views.FullContactBatchObj')
    @patch('fullcontact.views.DynamoDB')
    def test_upload_func(self, mock_fc, mock_db):
        file_name = os.path.join(settings.BASE_DIR, 'fullcontact/emails.csv')

        with open(file_name) as fp:
            upload_func(fp.read().encode('utf-8'))

        self.assertTrue(mock_fc.called)
        self.assertTrue(mock_db.called)

    @patch('fullcontact.views.SQSConnect.put_message')
    def test_sqs_push(self, mock_put_message):
        sqs_push(test_ticket, 'test')
        mock_put_message.assert_called_with('test', dumps(test_ticket))
        self.assertTrue(mock_put_message.called)

    def test_get_to_endpoint(self):
        response = self.client.get('/full-contact/webhook')
        self.assertEqual(403, response.status_code)

    @patch('fullcontact.views.django_rq')
    def test_post_to_webhook_endpoint(self, mock_django_rq):
        response = self.client.post(
            '/full-contact/webhook',
            {'result': dumps(test_ticket['fc']),
             'webhookId': '123456789'}
        )
        self.assertEqual(200, response.status_code)
        mock_django_rq.enqueue.assert_called_with(
            webhook_func,
            ticket_id='123456789',
            fc_result=dumps(test_ticket['fc'])
        )
        self.assertTrue(mock_django_rq.enqueue.called)

    @patch('fullcontact.views.django_rq')
    def test_post_to_batch_endpoint(self, mock_django_rq):
        response = self.client.post(
            '/full-contact/batch',
            {'result': dumps(test_ticket['fc']),
             'webhookId': 'test@test.com'}
        )
        self.assertEqual(200, response.status_code)
        mock_django_rq.enqueue.assert_called_with(
            batch_func,
            fc_callback_data='test@test.com',
            fc_result=dumps(test_ticket['fc'])
        )
        self.assertTrue(mock_django_rq.enqueue.called)

    def test_get_to_batch_emails_endpoint(self):
        response = self.client.get('/full-contact/batch-emails')
        form = UploadFileForm()
        expected_html = render_to_response('fullcontact/batch.html', {'form': form})
        expected = expected_html.content.decode('utf-8')
        received = response.content.decode('utf-8')

        self.assertEqual(expected, received)
        self.assertEqual(200, response.status_code)

    @patch('fullcontact.views.django_rq.queues.DjangoRQ')
    def test_post_to_batch_emails_endpoint(self, mock_enqueue):
        file_name = os.path.join(settings.BASE_DIR, 'fullcontact/emails.csv')

        with open(file_name) as fp:
            self.client.post(
                '/full-contact/batch-emails',
                {'name': 'file', 'file': fp}
            )

        self.assertTrue(mock_enqueue.called)


class TicketModelTest(TestCase):

    def test_saving_and_retrieving_tito_tickets(self):
        first_ticket = Ticket()
        first_ticket.ticket_id = '123456789'
        first_ticket.ticket = dumps({'Test': 'Dict'})
        first_ticket.save()

        saved_tickets = Ticket.objects.all()
        self.assertEqual(saved_tickets.count(), 1)

        test_ticket = Ticket.objects.get(ticket_id="123456789")
        self.assertEqual(test_ticket.ticket_id, '123456789')
        self.assertEqual(test_ticket.ticket, dumps({'Test': 'Dict'}))
