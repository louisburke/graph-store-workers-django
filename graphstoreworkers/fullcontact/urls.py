from django.conf.urls import url

from . import views

urlpatterns = [
    url(
        r'^webhook$',
        views.full_contact_webhook,
        name='full-contact-webhook'
    ),
    url(
        r'^batch$',
        views.full_contact_batch,
        name='full-contact-batch'
    ),
    url(
        r'^batch-emails$',
        views.batch_email_set,
        name='full-contact-batch-emails'
    ),
]
