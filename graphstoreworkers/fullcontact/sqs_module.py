from boto.sqs import (
    connect_to_region,
    message
)


class SQSConnect():
    def __init__(self, region, sqs_name):
        self.connection = connect_to_region(region)
        self.message_q = self.connection.get_queue(sqs_name)

    def get_messages(self, num_messages):
        message_dict = dict()
        message_list = list()
        messages = self.message_q.get_messages(num_messages=num_messages)

        for msg in messages:
            message_dict['body'] = msg.get_body()
            message_dict['attributes'] = msg.attributes
            message_list.append(message_dict)

        return message_list

    def put_message(self, message_attributes, message_body):
        if not type(message_attributes) is dict:
            raise TypeError('Message attributes need to be a dictionary.')
        else:
            new_message = message.RawMessage()
            new_message.set_body(message_body)
            new_message.message_attributes = message_attributes
            self.message_q.write(new_message)
            return('Success')
