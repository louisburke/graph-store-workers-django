from django.db import models


class Ticket(models.Model):
    ticket_id = models.TextField(default='')
    ticket = models.TextField(default='')


class BatchEmail(models.Model):
    email = models.TextField(default='', unique=True)
