import csv
import hashlib
import logging
from datetime import date
from io import StringIO
from json import loads, dumps

import boto
from boto.s3.connection import S3Connection

import django_rq
from django.db.utils import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.conf import settings

from requests import codes

from .forms import UploadFileForm
from .models import Ticket, BatchEmail
from .sqs_module import SQSConnect
from .wsfullcontact.wsfullcontact import FullContactBatchObj
from .wsdynamodb.wsdynamodb import DynamoDB

logger = logging.getLogger(__name__)


def get_s3_connection():
    try:
        return S3Connection()
    except boto.exception.NoAuthHandlerFound:
        logger.error("Can't connect to AWS!")
    except Exception as exe:
        logger.error("Error {}".format(exe))


def get_s3_path(data):
    json_dir = settings.JSON_DIR + '/' + str(date.today())
    ticket_json = dumps(data, sort_keys=True)
    hash_out = hashlib.sha512(ticket_json.encode('utf-8'))
    ticket_hash = hash_out.hexdigest()

    return "{}/{}.json".format(json_dir, ticket_hash)


def write_data_to_s3(data, bucket_name):
    connection = get_s3_connection()
    path = get_s3_path(data)
    bucket = connection.get_bucket(bucket_name)

    key = bucket.new_key(path)
    key.set_contents_from_string(dumps(data))


def batch_func(fc_result, fc_callback_data):
    sqs_dict = dict()
    try:
        batch_email_details_dict = loads(fc_callback_data)
        batch_email_object = BatchEmail.objects.get(
            email=batch_email_details_dict['email']
        )
        batch_email_object.delete()

        sqs_dict['fc'] = loads(fc_result)
        sqs_dict['tito'] = fc_callback_data

        logger.info(batch_email_details_dict['email'])
        write_data_to_s3(sqs_dict, settings.S3BUCKET)
        sqs_push(sqs_dict, 'batch-ticket')
    except ObjectDoesNotExist as exception:
        logger.error("Error {}".format(exception))
    except Exception as exception:
        logger.error("Error {}".format(exception))
        raise


def webhook_func(fc_result, ticket_id):
    sqs_dict = dict()
    try:
        ticket_object = Ticket.objects.get(ticket_id=ticket_id)
        ticket_details = ticket_object.ticket
        ticket_details_dict = loads(ticket_details)
        ticket_object.delete()

        sqs_dict['fc'] = loads(fc_result)
        sqs_dict['tito'] = ticket_details_dict

        logger.info(ticket_id)
        write_data_to_s3(sqs_dict, settings.S3BUCKET)
        sqs_push(sqs_dict, 'completed-ticket')
    except ObjectDoesNotExist as exception:
        logger.error("error {}".format(exception))
    except Exception as exception:
        logger.error("error {}".format(exception))
        raise


def upload_func(upload_file_buffer):
    try:
        name_email_list = []

        # Access Dynamo DB for credentials.
        ddb = DynamoDB('eu-west-1')
        cred = ddb.get(
            'cred_storage',
            'FullContact'
        )
        webhook_details = ddb.get(
            'webhook_storage',
            'gs_fullcontact_callback_batch'
        )

        # Get Full Contact interface object.
        fcbo = FullContactBatchObj(
            webhook_details['webhook_value'],
            cred['APIKey']
        )

        email_csv = loads(upload_file_buffer.decode('utf-8'))

        for row in email_csv:
            BatchEmail.objects.create(
                email=row['email']
            )
            name_email_list.append(
                {
                    'name': row['name'],
                    'email': row['email']
                }
            )
        fcbo.send_batch(name_email_list)
    except ObjectDoesNotExist as exception:
        logger.error("error {}".format(exception))
    except IntegrityError as exception:
        logger.error("error {}".format(exception))
    except Exception as exception:
        logger.error("error {}".format(exception))
        raise


def sqs_push(sqs_dict, attribute):
    try:
        sqs_obj = SQSConnect(settings.AWS_REGION, settings.AWS_QUEUE)
        sqs_obj.put_message(attribute, dumps(sqs_dict))
    except boto.exception.NoAuthHandlerFound:
        logger.error("Can't connect to AWS!")
    except Exception as exception:
        logger.error('{}'.format(exception))
        raise


dispatcher = {
    'batch': batch_func,
    'webhook': webhook_func
}


@csrf_exempt
def full_contact_webhook(request):
    if request.method == 'POST':
        data = request.POST['result']
        webhook_id_uuid = request.POST['webhookId']
        django_rq.enqueue(dispatcher['webhook'],
                          fc_result=data,
                          ticket_id=webhook_id_uuid)
        message = 'Endpoint Executed.'
        status = 200
    else:
        message = 'Forbidden.'
        status = 403

    return HttpResponse(message, status=status)


@csrf_exempt
def full_contact_batch(request):
    if request.method == 'POST':
        data = request.POST['result']
        webhook_id_uuid = request.POST['webhookId']
        django_rq.enqueue(dispatcher['batch'],
                          fc_result=data,
                          fc_callback_data=webhook_id_uuid)
        message = 'Endpoint Executed.'
        status = 200
    else:
        message = 'Method not supported.'
        status = 403

    return HttpResponse(message, status=status)


@csrf_exempt
def batch_email_set(request):
    if request.method == 'GET':
        form = UploadFileForm()
        message = 'GET method executed.'
        return render_to_response(
            'fullcontact/batch.html',
            {'form': form}
        )
    elif request.method == 'POST':
        try:
            uploaded_file = request.FILES['file']
            uploaded_file_str = uploaded_file.read()
            queue = django_rq.get_queue('batch')
            queue.enqueue(
                upload_func,
                upload_file_buffer=uploaded_file_str
            )
            message = 'Emails uploaded for work.'
            return_val = HttpResponse(message, status=codes.ok)
        except Exception as exception:
            raise exception

        return return_val
    else:
        print(request.method)
        message = 'Method not supported.'
        return HttpResponse(message, status=codes.FORBIDDEN)
