import logging
import requests
import redis
import sys
import traceback

from furl import furl
from time import sleep
from json import dumps
from uuid import uuid4


def chunks(l, n):
    """ Yield successive n-sized chunks from l.
    """
    for i in range(0, len(l), n):
        yield l[i:i+n]


class FullContactBatchObj:
    def __init__(self,
                 webhook_value,
                 fc_api_key,
                 redis_host='localhost',
                 redis_port=6379):

        # Set up URL for posting batch request.
        self.fc = furl('https://api.fullcontact.com')
        self.fc.path = '/v2/batch.json'
        self.fc.args['apiKey'] = fc_api_key

        # Set up person URL that is included in the batch request URL.
        self.person = furl('https://api.fullcontact.com')
        self.person.path = '/v2/person.json'
        self.person.args['webhookUrl'] = webhook_value

        # Set up individual URL for making an individual query to FC.
        self.individual = furl('https://api.fullcontact.com')
        self.individual.path = '/v2/person.json'
        self.individual.args['apiKey'] = fc_api_key
        self.individual.args['webhookUrl'] = webhook_value

        self.logger = logging.getLogger(__name__)
        # Set up a connection to Redis Cache for storing value to the
        # webhookId key.
        self.redis_db = redis.StrictRedis(host=redis_host,
                                          port=redis_port,
                                          db=0)

    # Query an individual from FC using there email and store their
    # corresponding details(message parameter) in a Redis Cache.
    def send_individual(self, email, message):
        # Key for key : value pair, where the value is the message.
        # The key is set as the webhookId so when the FC webhook call-back
        # is received the message can be retrieved from the Redis Cache.
        key = str(uuid4())

        self.redis_db.set(key, dumps(message))

        self.individual.args['email'] = email
        self.individual.args['webhookId'] = key

        try:
            requests.post(self.individual.url)
        except Exception as err:
            print(traceback.format_exc())

    # This function takes a list of emails of arbitrary length
    # and sends them to FC in batches of 20 using FC's
    # batch request functionality.
    def send_batch(self, emails, conf_slug='mail'):
        payload = {}
        payload_urls = []
        count = 0
        # Create a list of lists each 20 elements in length.
        slice_emails = list(chunks(emails, 20))

        web_hook_dict = {'conf_slug': conf_slug,
                         'email': ''}
        for _slice in slice_emails:
            count += len(_slice)
            print(count)
            if count > 580:
                count = 0
                sleep(60)
            # Build batch FC URL from the email list slice.
            for email in _slice:
                self.person.args['email'] = email['email']
                web_hook_dict['email'] = email['email']
                web_hook_dict['name'] = email['name']
                self.person.args['webhookId'] = dumps(web_hook_dict)

                payload_urls.append(self.person.url)

            payload['requests'] = payload_urls
            headers = {'Content-type': 'application/json'}
            try:
                ret = requests.post(self.fc.url,
                                    headers=headers,
                                    data=dumps(payload))
            except Exception as err:
                print(traceback.format_exc())

            payload_urls = []
            payload = {}
