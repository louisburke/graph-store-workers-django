#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv
import sys
from json import dumps
import boto.sqs as sqs
from boto.sqs.message import RawMessage

# maps SQS message fields (the keys) to Tito CSV dump fields (the values)
translate_map = {
    "release": "Ticket",
    "email": "Ticket Email",
    "first_name": "Ticket First Name",
    "last_name": "Ticket Last Name",
    "name": "Ticket Full Name",
    "price": "Price",
    "reference": "Ticket Reference",
    "slug": "Unique Ticket URL",
    "country": "Country",
}

# make sure we only merge to valid conferences
valid_conf_keys = ['cc14', 'cc15', 'ws13',
                   'ws14', 'ws15', 'entc15',
                   'monc15', 'rise15', 'mail', ]


def row_to_sqs(row, confslug):
    message_body = {}
    for k in translate_map:
        message_body[k] = row[translate_map[k]]

    message_body['conf_slug'] = confslug
    message_body['slug'] = message_body['slug'].split('/')[-1]
    message_body['email'] = message_body['email'].lower()
    if message_body['email'] not in ['', '-']:
        message = {'message': message_body, }
        mt_raw = dumps(message)
        mt = RawMessage()
        mt.set_body(mt_raw)
        QUEUE.write(mt)


if __name__ == "__main__":
    if len(sys.argv) < 2 or sys.argv[2] not in valid_conf_keys:
        raise SystemExit(
            ("Usage: python3 csv_to_sqs.py CSVFILE CONFSLUG\n"
             "(valid confslugs: {})")
            .format(', '.join(valid_conf_keys)))

    # AWS set up: connections and queues
    SQS_QUEUE = 'ticket_worker'
    CONN = sqs.connect_to_region('eu-west-1',)
    QUEUE = CONN.get_queue(SQS_QUEUE)
    conf_slug = sys.argv[2]
    with open(sys.argv[1]) as cin:
        cin = csv.DictReader(cin)
        for row in cin:
            print("{}\t{}".format(conf_slug, row['Ticket Reference']))
            row_to_sqs(row, conf_slug)
