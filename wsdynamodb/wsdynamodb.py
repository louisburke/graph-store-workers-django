from boto import dynamodb


class DynamoDB(object):

    def __init__(self, region):
        self._layer2 = dynamodb.connect_to_region(region)
        self._tables_by_name = {}

    def table_check(self, table_name):
        table = self._tables_by_name.get(table_name)
        if table is None:
            table = self._layer2.get_table(table_name)
            self._tables_by_name[table_name] = table
        return table

    def add(self, table_name, key, data):
        table = self.table_check(table_name)
        item = table.new_item(key, data)
        item.put()

    def get(self, table_name, key):
        table = self.table_check(table_name)
        q = table.query(key)
        # The following code assumes that this
        # hash key is unique i.e. only one item
        # is returned.
        return q.response['Items'][0]

    def delete(self, table_name, key):
        table = self.table_check(table_name)
        query = table.query(key)
        for item in query:
            item.delete()
