import csv
import requests
from datetime import datetime
from json import dump, dumps
from time import time, sleep
from furl import furl
from wsdynamodb.wsdynamodb import DynamoDB
from wsfullcontact.wsfullcontact import FullContactBatchObj

# Access Dynamo DB for credentials.
ddb = DynamoDB('eu-west-1')
cred = ddb.get('cred_storage', 'FullContact')
webhook_details = ddb.get('webhook_storage', 'gs_fullcontact_callback_batch')

# Get Full Contact interface object.
fcbo = FullContactBatchObj(webhook_details['webhook_value'], cred['APIKey'])

batch_list = list()
with open('emails.csv') as csvfile:
    READER = csv.DictReader(csvfile)
    for row in READER:
        batch_dict = {}
        ts = time()
        batch_dict['name'] = row['First_Name'] + ' ' + row['Last_Name']
        batch_dict['email'] = row['Email']
        batch_dict['timestamp'] = ts
        batch_dict['batch_id'] = "arbitrary"
        batch_list.append(batch_dict)


fc = furl('https://ds-graph-worker.websummit.net')
fc.path = '/full-contact/batch-emails'

with open('tests.csv', 'w+') as testfile:
    dump(batch_list, testfile)

# n = 2750
# for i in range(0, len(batch_list), n):
#     resp = requests.post(fc.url, data=dumps(batch_list[i:i+n]))
#     while 504 == resp.status_code:
#         sleep(1.5)
#         resp = requests.post(fc.url, data=dumps(batch_list[i:i+n]))
#         print('retry: ', resp.status_code)

# # Send batches to FC
# fcbo.send_batch(name_email_list)
